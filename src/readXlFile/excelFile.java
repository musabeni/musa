package readXlFile;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.sl.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class excelFile {
	
	private static Workbook wb;
	@SuppressWarnings("rawtypes")
	private static org.apache.poi.ss.usermodel.Sheet sh;
	private static FileInputStream fis;
	private static FileOutputStream fos;
	private static Cell cll,cll2,cll3;
	private static Row row;
	static  String phonenumber;
	static  String network;
	  static String number;

	@SuppressWarnings("rawtypes")
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		fis = new FileInputStream ("mobile.xlsx");
		wb = WorkbookFactory.create(fis);
		sh =  wb.getSheet("Sheet1");
	       int numOfRow = ((org.apache.poi.ss.usermodel.Sheet) sh).getLastRowNum();
	       
	       for(int i =0;i<numOfRow;i++) {
	    	   number = ( sh.getRow(i+1 ).getCell(0)).toString().trim(); 
	    	  if(number.isEmpty()) {
	    		  continue;
	    	  }
	    	  //get number from excel sheet
	    	  ArrayList<Character> listnumber = getCharc(number.replaceAll("\\s",""));
	    	  //take zero out
	    	  ArrayList<Character> outZero = removezero(listnumber);
	    	  //return number
	    	  returnNumber(outZero);
	    	  //System.out.println(number); 
	    	  if(phonenumber.trim().length()==12) {
	    		 // System.out.println(phonenumber+" "+network); 
	    		  writeToexcel(phonenumber,network,i+1,number);
	    	  }else {
	    		  network = "WRONG NUMBER";
	    		 // System.out.println(phonenumber+" "+"INVALID PHONE NUMBER");
	    		  writeToexcel(phonenumber,network,i+1,number);
	    	  }
	  		
	    	 
    	  
    	 
	       }
	       System.out.print(numOfRow);
	}
	
	public static void writeToexcel(String number,String operator,int numRow,String ognumber) throws IOException {
		//row=sh.createRow(numRow);
		
			row=sh.createRow(numRow);
			cll2 = row.createCell(0);
			cll2.setCellValue(ognumber);
			cll = row.createCell(1);
			cll.setCellValue(number);
			cll3 = row.createCell(2);
			cll3.setCellValue(operator);
			System.out.println(cll2.getStringCellValue()+" "+cll.getStringCellValue()+" "+cll3.getStringCellValue());
			//cll = row.createCell(2);
			try {
				fos = new FileOutputStream("mobile.xlsx");
				wb.write(fos);
				
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
//		fos.flush();
//		fos.close();
	}
	
public static ArrayList<Character> getCharc(String n) {
		
		char[] nums = n.toCharArray();
		ArrayList<Character> list = new ArrayList();
		char[] getnums = new char[nums.length];
		for(int i=0;i<nums.length;i++) {
			if(Character.toString(nums[i]).equals("+") || Character.toString(nums[i]).equals(".")|| Character.toString(nums[i]).equals("-")
					||Character.toString(nums[i]).equals("=")||Character.toString(nums[i]).equals("(")
					||Character.toString(nums[i]).equals(")")||Character.toString(nums[i]).equals(",")) {
				continue;
			}
			
			list.add(nums[i]);
			
			
			
		}
		
		return list;
		
	}

//remove zero
	public static ArrayList<Character> removezero(ArrayList<Character> listnumber){
		
char[] ch = new char[listnumber.size()];
ArrayList<Character> lists = new ArrayList();	
		for(int x=0;x<listnumber.size();x++) {
			if(x<listnumber.size()) {
				ch[x]=listnumber.get(x);
			}
				
			if(new String(ch).trim().equals("2550") && Character.toString(listnumber.get(x)).equals("0") ) {
				continue;
			}
			lists.add(listnumber.get(x));
			//System.out.println(listnumber.get(x));	
		}
		return lists;
	}
	
	
	//return complete number
	
	public static void returnNumber(ArrayList<Character> outZero) {
		char[] completeNumber = new char[outZero.size()];
		if(Character.toString(outZero.get(0)).equals("0")) {
			outZero.remove(0);
		}
		
		
		
		for(int z=0;z<outZero.size();z++) {
			
			completeNumber[z]=outZero.get(z);
			
		}
		if(Character.toString(completeNumber[0]).equals("2")&&Character.toString(completeNumber[1]).equals("5")&&Character.toString(completeNumber[2]).equals("5")) {
			if(Character.toString(completeNumber[3]).equals("7")&&Character.toString(completeNumber[4]).equals("1")) {
				network = "TIGO";
			}else if(Character.toString(completeNumber[3]).equals("6")&&Character.toString(completeNumber[4]).equals("5")) {
				network = "TIGO";
			}else if(Character.toString(completeNumber[3]).equals("7")&&Character.toString(completeNumber[4]).equals("5")) {
				network = "VODACOM";
			}else if(Character.toString(completeNumber[3]).equals("7")&&Character.toString(completeNumber[4]).equals("6")) {
				network = "VODACOM";
			}else if(Character.toString(completeNumber[3]).equals("2")) {
				network = "TTCL";
			}else if(Character.toString(completeNumber[3]).equals("7")&&Character.toString(completeNumber[4]).equals("8")) {
				network = "AIRTEL";
			}else if(Character.toString(completeNumber[3]).equals("6")&&Character.toString(completeNumber[4]).equals("8")) {
				network = "AIRTEL";
			}else if(Character.toString(completeNumber[3]).equals("7")&&Character.toString(completeNumber[4]).equals("7")) {
				network = "ZANTEL";
			}else if(Character.toString(completeNumber[3]).equals("6")&&Character.toString(completeNumber[4]).equals("2")) {
				network = "HALOTEL";
			}else if(Character.toString(completeNumber[3]).equals("6")&&Character.toString(completeNumber[4]).equals("7")) {
				network = "TIGO";
			}else {
				network="UNKNOWN";
			}
			phonenumber = new String(completeNumber);
		}else {
			if(Character.toString(completeNumber[0]).equals("7")&&Character.toString(completeNumber[1]).equals("1")) {
				network = "TIGO";
			}else if(Character.toString(completeNumber[0]).equals("6")&&Character.toString(completeNumber[1]).equals("5")) {
				network = "TIGO";
			}else if(Character.toString(completeNumber[0]).equals("7")&&Character.toString(completeNumber[1]).equals("5")) {
				network = "VODACOM";
			}else if(Character.toString(completeNumber[0]).equals("7")&&Character.toString(completeNumber[1]).equals("6")) {
				network = "VODACOM";
			}else if(Character.toString(completeNumber[0]).equals("2")) {
				network = "TTCL";
			}else if(Character.toString(completeNumber[0]).equals("7")&&Character.toString(completeNumber[1]).equals("8")) {
				network = "AIRTEL";
			}else if(Character.toString(completeNumber[0]).equals("6")&&Character.toString(completeNumber[1]).equals("8")) {
				network = "AIRTEL";
			}else if(Character.toString(completeNumber[0]).equals("7")&&Character.toString(completeNumber[1]).equals("7")) {
				network = "ZANTEL";
			}else if(Character.toString(completeNumber[0]).equals("6")&&Character.toString(completeNumber[1]).equals("2")) {
				network = "HALOTEL";
			}else if(Character.toString(completeNumber[0]).equals("6")&&Character.toString(completeNumber[1]).equals("7")) {
				network = "TIGO";
			}else {
				network="UNKNOWN";
			}
			phonenumber ="255"+new String(completeNumber);
		}
	}	
	


}

